﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRIZIC_KRUZIC_Csharp
{
    struct Place
    {
        public int x;
        public int y;
    };
    public partial class Form1 : Form
    {
        Game game;
        int size;
        Graphics g;
        string name1, name2;
        int n, win1, win2;
        public Form1()
        {
            n=new int();
            win1 = new int();
            win2 = new int();

            InitializeComponent();
            size = pictureBox1.Size.Width;
            this.pictureBox1.Size = new System.Drawing.Size(size, size);
            Console.WriteLine(size);
            g = pictureBox1.CreateGraphics();
            game = new Game(0);
            if (textBox_name1.Text.ToString() == "") textBox_name1.Text = "Igrač 1";
            if (textBox_name2.Text.ToString() == "") textBox_name2.Text = "Igrač 2";
            name1 = textBox_name1.Text.ToString();
            name2 = textBox_name2.Text.ToString();
            who_plays.Text = textBox_name1.Text;
        }
        private void new_games_Click(object sender, EventArgs e)
        {
            n = 0;
            win1 = 0;
            win2 = 0;
            game = new Game(0);
            name1 = textBox_name1.Text.ToString();
            name2 = textBox_name2.Text.ToString();
            pictureBox1.Image = null;
            rezultat.Text = "";
            who_plays.Text = name1;
        }

        private void Mouse_click(object sender, MouseEventArgs e)
        {
            Place location = new Place();
            location = determine_place(e);
            
            if (location.x == -1) return;
            if (!game.taken(location))
            {
                game.mtaken[Convert.ToInt32(game.player_turn), location.x, location.y] = true;
                game.player_turn = !game.player_turn;
                draw_shape(game.player_turn, location);
            }
            int x = new int();
            x = game.check_end();
            if (x!=0)
            {
                string ending_mess = "Pobijedio je ";
                 n++;
                game = new Game(n);
               
                if (1 == x)
                {
                    win1++;
                    ending_mess += name1;
                    //if (name1 == "") ending_mess += "igrač 1";
                    ending_mess += ".";
                    MessageBox.Show(ending_mess, "Pobjeda");
                }
                if (2 == x)
                {
                    win2++;
                    ending_mess += name2;
                    //if (name2 == "") ending_mess += "igrač 2";
                    ending_mess += ".";
                    MessageBox.Show(ending_mess, "Pobjeda");
                }
                if (3 == x)
                {
                    MessageBox.Show("Neriješeno", "Neriješeno");
                }
                string rez = "";
                rez += win1.ToString();
                rez += ":";
                rez += win2.ToString();
                rezultat.Text = rez;
                pictureBox1.Image = null;
                if (game.player_turn)
                {
                    who_plays.Text = name2;
                }
                else
                {
                    who_plays.Text = name1;
                }
                return;
            }
            if (game.player_turn)
            {
                who_plays.Text = name2;
            }
            else
            {
                who_plays.Text = name1;
            }
        }

        private Place determine_place(MouseEventArgs e)
        {
            Place ret=new Place();
            ret.x = -1;
            ret.y = -1;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (e.X > (size/3 * 0.05+i* size/3) && e.X < (size/3 * 0.95 + i * size /3))
                    {
                        if (e.Y > (size /3 * 0.05+j*size/3) && e.Y < (size /3 * 0.95 + j * size /3))
                        {
                            ret.x = i;
                            ret.y = j;
                        }
                    }
                }
            }
            return ret;
        }
        private void draw_shape(bool player, Place p)
        {
            Pen P = new Pen(Color.Red, 20F);
            if (player == false)
            {
                PointF point1 = new PointF(size / 3 * p.x+size/21, size/3*p.y+size/21);
                PointF point2 = new PointF(size / 3 * p.x + 6 * size / 21, size / 3 * p.y + 6 * size / 21);
                g.DrawLine(P, point1, point2);
                point1 = new PointF(size / 3 * p.x + 6 * size / 21, size / 3 * p.y + size / 21);
                point2 = new PointF(size / 3 * p.x + size / 21, size / 3 * p.y + 6 * size / 21);
                g.DrawLine(P, point1, point2);

            }
            else
            {
                g.DrawEllipse(P, p.x*size/3+size/21, p.y*size/3+size/21, 5*size/21, 5*size/21);
            }
        }
    }

    class Game
    {
        public bool player_turn;
        public bool[,,] mtaken;
        public Game(int num)
        {
            if (num % 2 == 1) player_turn = true;
            else player_turn = false;
            mtaken = new bool[2, 3, 3];
        }
        public bool taken(Place p)
        {
            return mtaken[0, p.x, p.y] || mtaken[1, p.x, p.y];
        }
        public bool tryplace(bool player, Place p)
        {
            if (taken(p) == false)
            {
                mtaken[Convert.ToInt32(player), p.x, p.y] = true;
                return true;
            }
            return false;
        }
        public int check_end()
        {
            //vraca 0 ako nije gotovo
            //vraca 1 ako player"0"
            //vraca 2 ako player"1"
            //vraca 3 ako nerjeseno
            int count1 = new int();
            int count2 = new int();
            int count3 = new int();
            for (int player = 0; player < 2; player++)
            {
                for (int i = 0; i < 3; i++)
                {
                    count1 = 0;
                    count2 = 0;
                    for (int j = 0; j < 3; j++)
                    {
                        if (mtaken[player, i, j] == true) count1++;
                        if (mtaken[player, j, i] == true) count2++;
                        if (mtaken[player, i, j] == true) count3++;
                    }
                    if (count1 == 3)
                    {
                        return player + 1;
                    }
                    if (count2 == 3)
                    {
                        return player + 1;
                    }
                }
                count1 = 0;
                count2 = 0;
                for (int i = 0; i < 3; i++)
                {
                    if (mtaken[player, i, i] == true) count1++;
                    if (mtaken[player, i, 2 - i] == true) count2++;
                }
                if (count1 == 3) return player+1;
                if (count2 == 3) return player+1;
            }
            if (count3 == 9) return 3;
            return 0;
        }
        public void print_game()
        {
            for (int player = 0; player < 2; player++)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        Console.Write(Convert.ToInt32(mtaken[player, j, i]).ToString());
                        Console.Write("  ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}
